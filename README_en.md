The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and "OPTIONAL" in this document are to be interpreted as described in RFC 2119.

Before each GIT commit you MUST follow and stick on this tutorial: https://git-scm.com/book/en/v2 in order to setup your git username (name and surname) and your email (xxx@wellnet.it). E.g.:

```
git config --global user.name "Luca Lusso"
git config --global user.email "luca.lusso@wellnet.it"
```

# Server

Actually we are using two Git hosting providers:

* Bitbucket → repository for private and internal code
* Github → repository for public code

# Bitbucket

Bitbucket repositories MUST be named as follow:

* if the repository contains a website, it MUST have the complete name of the official domain where the production environment is hosted
* if the repository contains a web application, it MUST have the name of the application

Bitbucket allows an easy rename of the repository in case of domain change.

# Git

Keywords in this section:

* id_issue: id of the issue on Jira, e.g. WWW-1
* title_issue: title of the issue on Jira, e.g. update-size change the font size in homepage
* description_issue: description of the changes applied with the commit, e.g. Add new font attribute for the article  title

The .gitignore file MUST be set like this: .gitignore for Drupal

Branches and comments:

* MUST be created a new branch for each issue (workflow details here: https://www.atlassian.com/git/workflows) and every issue that includes code changes (also features) MUST use the label FEATURE, HOTFIX or UPGRADE in order to be clear for a reader what type of changes are in the branch
* comments MUST be in the format "id_issue title_issue - description_issue", if the commit includes more that one issue, the comment will contain one line per issue
* description_issue MUST start with a capital letter and MUST NOT have a dot at the end of the line
* in the exceptional event when there is a commit unlinked to a Jira task, the format MUST use this format: "NOID NOISSUE - Description_issue"
* description_issue SHOULD follow this rule: a correct and well formatted commit description SHOULD always complete the sentence: "if applied, this commit will <description_issue>"

Exempli gratia:

```
AAA-1 Fix the error Y - Remove the incorrect file inclusion
BBB-42 Add functionality Z - Add the content type W and the view K
```

Type of branches:

* master: contains the last version of the code. It does not necessarily have to be in production yet. The code MUST be working
* stage: contans all the new features and changes that are ready to be loaded into production. The code MUST be working
* dev: contains the "work in progress" features and changes. The code in this branch MAY be not working.
* FEATURE/: contains the code of one (and only one) new feature in developing. It's created from master. The ONLY way to move a piece of code from a FEATURE branch to the branches dev, stage or master is using the Pull Request mechanism.
    * Before open a PR, the branch MUST be merged from master (and ONLY master, NEVER merge stage or dev branches on this branch)
    * IF the FEATURE depends on another FEATURE already developed but NOT merged in master branch, the new FEATURE will created starting from master and will be updated, via pull, teh FEATURE from wich depends
    * Before every commit and/or a pull request, SHOULD merge the master branch into the new FEATURE and pull all the dependencies branches (and ONLY master, NEVER merge stage or dev branches on this branch)
    * When a feature is merged via PR in the master branch, SHOULD be made a merge from master into dev and stage branches
* HOTFIX/: contains the code that fix a problem on master. Is created from master and all the pull request MUST be done only on master. Before the PR,it MUST merge the master branch into the HOTFIX branch (and ONLY master, NEVER merge stage or dev branches on this branch). When a HOTFIX is merged via PR in the master branch, SHOULD be made a merge from master into dev and stage branches with the deploy of the relevant environments.
* UPGRADE/: contains the update of code and libraries from third party libraries used in the repositiry. e.g. a contrib module in Drupal). The rules to follow are the same of FEATURE branches.

# Deploy

* every time a PR is merged in master (or an hotfix) a git tag MUST be created
* extract the last tag from master:

```
git describe --abbrev=0
```

* the tag MUST contains the list of all the issues worked and solved. The complete list of commits can be extracted usng the command (where v2.0.0 is the last tag created from master): 

```
git log v2.0.0..HEAD --pretty="%s%n%b"
```

* e.g. (remove all the lines that refers to merges, commit messages and duplicates):

```
WWW-700 SCHEDA CARICAMENTO AUTOVETTURE NUOVE
WWW-705 INSERIMENTO VETTURE NUOVE
```

* in order to create a new tag use the command:

```
git tag -a v2.0.1
```

* and push the new tag on the remote repository

```
git push origin --tags
```

* if an hotfix is applied, every tme a tag is created in production from master branch it MUST be merged with dev and stage

```
git checkout stage
git pull origin stage
git merge master
git push origin stage
git checkout dev
git pull origin dev
git merge master
git push origin dev
```

* every time a tag is created in production the internal documentation page about Versions must be manually updated.

# Resources

https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow
