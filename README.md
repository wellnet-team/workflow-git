Le parole chiave "DEVE" ("MUST"), "NON DEVE" ("MUST NOT"), "REQUIRED", "SHALL", "SHALL NOT", "DOVREBBE" ("SHOULD"), "SHOULD NOT", "RECOMMENDED", "PUO'" ("MAY"), e "OPTIONAL" in questo documento sono da interpretarsi come descritto nella RFC 2119.

Prima di fare un commit su GIT si DEVE seguire e applicare questo tutorial: https://git-scm.com/book/it/v1/Per-Iniziare-Prima-Configurazione-di-Git per impostare il proprio nome utente (nome cognome) e la propria email (xxx@wellnet.it), quindi ad esempio:

```
git config --global user.name "Luca Lusso"
git config --global user.email "luca.lusso@wellnet.it"
```

# Server

Abbiamo attualmente 2 provider su cui mettiamo codice con Git:

* Bitbucket → repository principale per il codice privato
* Github → repository principale per il codice open source

# Bitbucket

I repository su Bitbucket DEVONO avere il nome così composto :

* nel caso di un sito web DEVONO avere il nome intero del dominio su cui è ospitato il sito di produzione
* nel caso di un applicazione web DEVONO avere il nome dell'applicazione

Bitbucket da la possibilità di cambiare facilmente il nome del repository qualora il sito venisse spostato su un altro dominio.

# Git

Legenda:

* id_issue: id della issue su Jira, ad esempio WWW-1
* titolo_issue: titolo della issue su Jira, ad esempio free-size dettaglio articolo
* dettaglio modifica: descrizione della modifica fatta, ad esempio Creato css per gestire immagine articolo

Il .gitignore DEVE essere impostato come illustrato qua: .gitignore per Drupal

Rami e commenti:

* DEVE essere creato un ramo per ogni issue (per i dettagli del workflow vedere qua: https://www.atlassian.com/git/workflows) ogni issue che comporta modifiche al codice (features comprese) deve essere di tipo FEATURE, HOTFIX o UPGRADE in modo da caratterizzare il ramo corrispondente con la corretta etichetta
* i commenti DEVONO essere nella forma "id_issue titolo_issue - dettaglio modifica", se il commit coinvolge più di una issue il commento si ripete su più righe, una per ogni issue
* il dettaglio della modifica DEVE iniziare con una lettera maiuscola e NON DEVE avere un punto alla fine
* nel caso (raro) in cui un commit non sia legato ad un task DEVE essere usata la dicitura: "NOID NOISSUE - Dettaglio modifica"
* il dettaglio della modifica DOVREBBE seguire questa regola: un messaggio di commit scritto correttamente in GIT dovrebbe sempre essere in grado di completare la frase: "se applicato, questo commit <dettaglio della modifica>"

Esempi:

```
AAA-1 Correggere l'errore Y - Rimuove il codice errato
BBB-42 Implementare la funzionalità Z - Aggiunge il content type W e la vista K
```

Tipi di rami:

* master: contiene l'ultima versione del codice, non necessariamente ancora in produzione. Il codice in questo ramo DEVE essere funzionante
* stage: contiene l'integrazione di tutte le nuove funzionalità pronte per essere caricate in produzione. Il codice in questo ramo DEVE essere funzionante  
* dev: contiene l'integrazione delle funzionalità in lavorazione. Il codice in questo ramo PUO' non essere funzionante
* FEATURE/: contiene il codice di una (e una sola) nuova funzionalità in sviluppo. Viene creato a partire dal ramo master. Il codice su un ramo FEATURE viene integrato in dev, stage e master SOLO attraverso il meccanismo delle pull request 
    * Prima di fare un pull request DEVE essere riallineato con master (e SOLO master, non vanno MAI fatti merge da stage o dev su questo ramo)
    * SE la FEATURE dipende da una FEATURE precedentemente sviluppata ma NON ancora implementata nel ramo MASTER, la nuova FEATURE partirà dal ramo master e integrerà, via pull, la FEATURE da cui dipende
    * Prima di fare un commit e/o una pull request, BISOGNA riallineare la FEATURE con le proprie dipendenze e con master  (e SOLO master, non vanno MAI fatti merge da stage o dev su questo ramo)
    * Quando una feature viene integrata in master BISOGNA successivamente fare un merge del ramo master sui rami dev e stage
* HOTFIX/: contiene il codice che corregge un problema attualmente presente sul ramo master. Viene creato a partire dal ramo master e tutte le pull request DEVONO essere fatte solo sul ramo master. Prima di fare un pull request DEVE essere riallineato con master (e SOLO master, non vanno MAI fatti merge da stage o dev su questo ramo). Quando si fa la pull request di un ramo di tipo HOTFIX/ su master, è necessario chiedere contestualmente il merge dell'hotfix sui rami stage e dev (quest'ultimo solo se esistente) e il rilascio sui relativi ambienti
* UPGRADE/: contiene il codice di aggiornamento di eventuali librerie open source di terze parti presenti nel repository (ad esempio un modulo contrib di Drupal). Valgono le stesse regole e specifiche dei rami di tipo FEATURE

# Deploy

* tutte le volte che si integra stage (oppure un hotfix) dentro master si DEVE staccare un tag
* estrarre l'ultimo tag staccato sul ramo master:

```
git describe --abbrev=0
```

* il tag DEVE contenere la lista di tutte le issue che risolve. L'elenco completo dei commit si recupera con il comando (dove v2.0.0 è l'ultimo tag staccato sul ramo master): 

```
git log v2.0.0..HEAD --pretty="%s%n%b"
```

* ad esempio (rimuovere tutte le righe che riguardano i merge, i messaggi di commit e i doppioni):

```
WWW-700 SCHEDA CARICAMENTO AUTOVETTURE NUOVE
WWW-705 INSERIMENTO VETTURE NUOVE
```

* per creare il tag si usa il comando

```
git tag -a v2.0.1
```

* caricare il tag sul repo

```
git push origin --tags
```

* in caso di hotfix, tutte le volte che si stacca un tag in produzione il ramo master DEVE essere riportato indietro sui rami dev e stage

```
git checkout stage
git pull origin stage
git merge master
git push origin stage
git checkout dev
git pull origin dev
git merge master
git push origin dev
```

* tutte le volte che si stacca un tag in produzione bisogna aggiornare la pagina con le Versioni rilasciate

# Risorse

    https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow
